<!doctype html>
<html lang="fi">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
    <title>Kuvagalleria</title>
</head>

<?php
include 'lib/ImageResize.php';

use \Gumlet\ImageResize;
?>
<?php
if ($_FILES['file']['error'] === UPLOAD_ERR_OK) {
    if ($_FILES['file']['size'] > 0) {
        $type = $_FILES['file']['type'];
        if ($type === 'image/png' || $type === 'image/jpg' || $type === 'image/jpeg') {
            $file = basename($_FILES['file']['name']);
            $folder = 'uploads/';
            if (move_uploaded_file($_FILES['file']['tmp_name'], "$folder$file")) {
                try {
                    $image = new ImageResize("$folder$file");
                    $image->resizeToWidth(150);
                    $image->save($folder . 'thumbs/' . $file);
                    print "<p>Kuva on tallennettu palvelimelle!</p>";
                } catch (Exception $ex) {
                    print "<p>Kuvan tallennuksessa tapahtui virhe.</p>" . $ex->getMessage() . "</p>";
                }
            } else {
                print "<p>Kuvan tallennuksessa tapahtui virhe.</p>";
            }
        } else {
            print "<p>Voit ladata vain png- ja jpg-kuvia!</p>";
        }
    } else {
        print "<p>Tiedoston koko on 0!</p>";
    }
} else {
    print "<p>Virhe kuvan lataamisessa! Virhekoodi: " . $_FILES['file']['error'] . "</p>";
}
?>
<a href="index.php">Selaa kuvia</a>
<?php require_once 'inc/bottom.php'; ?>