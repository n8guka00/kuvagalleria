<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Contact us</title>
</head>

<body>
    <div class="container">
        <h3>Contact us:</h3>
        <div class="row">
            <div class="col-lg-12">
                <?php
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    try {
                        $db = new PDO('mysql:host=localhost;dbname=mydatabase;charset=utf8', 'root', '');

                        $sql = "insert into feedback (name, email, subject, message) values "
                            . "(:name, :email, :subject, :message)";

                        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
                        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
                        $subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_STRING);
                        $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

                        $statement = $db->prepare($sql);
                        $statement->bindValue('name', $name, PDO::PARAM_STR);
                        $statement->bindValue('email', $email, PDO::PARAM_STR);
                        $statement->bindValue('subject', $subject, PDO::PARAM_STR);
                        $statement->bindValue('message', $message, PDO::PARAM_STR);

                        $statement->execute();
                        print "<p class='badge badge-info'>Thank you for your feedback!</p>";
                    } catch (Exception $ex) {
                        print "<p>Failure in database connection." . $ex->getMessage() . "</p>";
                    }
                }
                ?>
                <form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post"></form>
                <div>
                    <label>Name:</label>
                    <input type="text" name="name">
                </div>
                <div>
                    <label>Email:</label>
                    <input type="email" name="email">
                </div>
                <div>
                    <label>Subject:</label>
                    <input type="text" name="subject">
                </div>
                <div>
                    <label>Message:</label>
                    <textarea class="form-control" name="message" rows="3"></textarea>
                </div>
                </form>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>